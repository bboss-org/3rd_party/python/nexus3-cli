---
include:
  - template: Code-Quality.gitlab-ci.yml
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: License-Scanning.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml
  - template: Secret-Detection.gitlab-ci.yml
#
variables:
  SAST_BANDIT_EXCLUDED_PATHS: '*/tests/*'
#
stages:
  - build
  - test
  - quality
  - publish
  - trigger
# --------------------------------------------------------------------------
# `build` stage
# --------------------------------------------------------------------------
.unit_template: &unit_template
  before_script:
    - pip install -e .[test]
  variables:
    PYTHON_VERSION: '3.8'
  image: python:${PYTHON_VERSION}
  stage: test
  script:
    - pytest -m 'not integration'
      --junitxml=junit-${PYTHON_VERSION}.xml
      --cov-report xml:coverage-${PYTHON_VERSION}.xml
  coverage: '/TOTAL.+ ([0-9]{1,3}%)/'
  artifacts:
    paths:
      - coverage*.xml
      - junit*.xml
    reports:
      junit: junit*.xml
  dependencies: []
#
flake8:
  <<: *unit_template
  image: python:${PYTHON_VERSION}-alpine
  stage: build
  artifacts: {}
  before_script: []
  script:
    - pip install flake8
    - flake8 *.py src tests --format=pylint
#
mypy:
  <<: *unit_template
  stage: build
  artifacts: {}
  script:
    - pip install mypy
    - mypy src
#
package:
  <<: *unit_template
  stage: build
  before_script: []
  script:
    - python setup.py sdist bdist_wheel
  artifacts:
    paths:
      - dist
# --------------------------------------------------------------------------
# `test` stage
# --------------------------------------------------------------------------
# unit tests
unit-py3.6:
  <<: *unit_template
  variables:
    PYTHON_VERSION: '3.6'
#
unit-py3.7:
  <<: *unit_template
  variables:
    PYTHON_VERSION: '3.7'
#
unit-py3.8:
  <<: *unit_template
  variables:
    PYTHON_VERSION: '3.8'
#
# integration tests
.integration_template: &integration_template
  <<: *unit_template
  before_script:
    - docker info
    # get nexus going since it takes a little while to start-up
    - docker run -d --rm -p 8081:8081 -v $(pwd)/tests/fixtures/nexus-data/etc:/nexus-data/etc --name nexus sonatype/nexus3:$NEXUS_VERSION
    # test dependencies
    - apk add bash curl py3-pip python3-dev gcc musl-dev linux-headers openssl-dev libffi-dev
    - export CFLAGS=-I/usr/include
    - pip3 install dist/nexus3_cli*.whl
    # don't use --upgrade; we want to test the package as installed above
    - pip3 install .[test]
    # wait until the nexus service is ready to receive requests
    - ./tests/wait-for-nexus.sh http://docker:8081 || exit 1
    - nexus3 login -U http://docker:8081 --no-x509_verify -u admin -p $(docker exec nexus cat /nexus-data/admin.password)
  image: docker:latest
  services:
    - docker:dind
  stage: test
  script:
    - pytest -m integration
      --junitxml=junit-integration-${NEXUS_VERSION}.xml
      --cov-report xml:coverage-integration-${NEXUS_VERSION}.xml
  dependencies:
    - package
  except: ['tags']
# integration tests
int-nexus-3.25:
  <<: *integration_template
  variables:
    NEXUS_VERSION: '3.25.1'
#
int-nexus-3.26:
  <<: *integration_template
  variables:
    NEXUS_VERSION: '3.26.1'
#
int-nexus-latest:
  <<: *integration_template
  variables:
    NEXUS_VERSION: 'latest'
# --------------------------------------------------------------------------
# `quality` stage
# --------------------------------------------------------------------------
spotbugs-sast:
  rules:
    - when: never
# --------------------------------------------------------------------------
# `publish` stage
# --------------------------------------------------------------------------
coverage:
  <<: *unit_template
  before_script: []
  artifacts: {}
  dependencies:
    - unit-py3.6
    - unit-py3.7
    - unit-py3.8
    - int-nexus-3.25
    - int-nexus-3.25
    - int-nexus-latest
  image: python:${PYTHON_VERSION}-alpine
  stage: publish
  script:
    - pip install codecov
    - codecov
  when: always
  except: ['tags']
#
pypi:
  stage: publish
  image: python:3
  script:
    - pip install -U twine
    - twine upload dist/*
  only: ['tags']
#
docker:
  stage: trigger
  trigger:
    project: thiagocsf/docker-nexus3-cli
    branch: master
  only: ['master', 'tags']
